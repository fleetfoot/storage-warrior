import { src, dest, watch, series, parallel } from "gulp";
import yargs from "yargs";
import sass from "gulp-sass";
import cleanCss from "gulp-clean-css";
import gulpif from "gulp-if";
import postcss from "gulp-postcss";
import sourcemaps from "gulp-sourcemaps";
import autoprefixer from "autoprefixer";
import imagemin from "gulp-imagemin";
import del from "del";
import webpack from "webpack-stream";
import named from "vinyl-named";
import browserSync from "browser-sync";
import wpPot from "gulp-wp-pot";
const PRODUCTION = yargs.argv.prod;

const server = browserSync.create();
export const serve = done => {
    server.init({
        proxy: "http://localhost:8060",
        open: false
    });
    done();
};
export const reload = done => {
    server.reload();
    done();
};
export const clean = () => del(["wp-content/themes/storage-warrior/dist"]);

export const styles = () => {
    return src(["src/scss/index.scss", "src/scss/editor-styles.scss"])
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(sass().on("error", sass.logError))
        .pipe(gulpif(PRODUCTION, postcss([autoprefixer])))
        .pipe(gulpif(PRODUCTION, cleanCss({ compatibility: "ie8" })))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write()))
        .pipe(dest("wp-content/themes/storage-warrior/dist/css"))
        .pipe(server.stream());
};

export const images = () => {
    return src("src/images/**/*.{jpg,jpeg,png,svg,gif}")
        .pipe(gulpif(PRODUCTION, imagemin()))
        .pipe(dest("wp-content/themes/storage-warrior/dist/images"));
};

export const copy = () => {
    return src([
        "src/**/*",
        "!src/{images,js,scss}",
        "!src/{images,js,scss}/**/*"
    ]).pipe(dest("wp-content/themes/storage-warrior/dist"));
};

export const scripts = () => {
    return src(["src/js/index.js"])
        .pipe(named())
        .pipe(
            webpack({
                module: {
                    rules: [
                        {
                            test: /\.js$/,
                            use: {
                                loader: "babel-loader",
                                options: {
                                    presets: []
                                }
                            }
                        }
                    ]
                },
                mode: PRODUCTION ? "production" : "development",
                devtool: !PRODUCTION ? "inline-source-map" : false,
                output: {
                    filename: "[name].js"
                },
                externals: {
                    jquery: "jQuery"
                }
            })
        )
        .pipe(dest("wp-content/themes/storage-warrior/dist/js"));
};

export const pot = () => {
    return src("**/*.php")
        .pipe(
            wpPot({
                domain: "_storagewarrior",
                package: info.name
            })
        )
        .pipe(dest(`languages/${info.name}.pot`));
};
export const watchForChanges = () => {
    watch("src/scss/**/*.scss", styles);
    watch("src/images/**/*.{jpg,jpeg,png,svg,gif}", series(images, reload));
    watch(
        ["src/**/*", "!src/{images,js,scss}", "!src/{images,js,scss}/**/*"],
        series(copy, reload)
    );
    watch("src/js/**/*.js", series(scripts, reload));
    watch("wp-content/themes/storage-warrior/**/*.php", reload);
};
export const dev = series(
    clean,
    parallel(styles, images, copy, scripts),
    serve,
    watchForChanges
);
export const build = series(clean, parallel(styles, images, copy, scripts));
export default dev;
