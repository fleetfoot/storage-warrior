(function($) {
    var slider = $("#sw-slider");
    slider.css("visibility", "visible");

    if (slider.length) {
        $("#sw-slider").slick({
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: false
        });
    }
})(jQuery);
