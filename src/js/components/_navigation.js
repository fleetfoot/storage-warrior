/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function() {
    var site, container, button, close, menu, links, i, len;

    site = document.getElementById("page");
    if (!site) {
        return;
    }

    container = document.getElementById("site-navigation");
    if (!container) {
        return;
    }

    button = document.getElementById("hamburger-open");
    if ("undefined" === typeof button) {
        return;
    }

    close = document.getElementById("hamburger-close");
    if ("undefined" === typeof close) {
        return;
    }

    menu = container.getElementsByTagName("ul")[0];

    // Hide menu toggle button if menu is empty and return early.
    if ("undefined" === typeof menu) {
        button.style.display = "none";
        return;
    }

    if (-1 === menu.className.indexOf("nav-menu")) {
        menu.className += " nav-menu";
    }

    button.onclick = function() {
        window.scrollTo(0, 0);
        container.className += " toggled";
        button.setAttribute("aria-expanded", "true");

        site.className += " menu-toggled";
        document.body.className += " menu-toggled--body";
    };

    close.onclick = function() {
        container.className = container.className.replace(" toggled", "");
        button.setAttribute("aria-expanded", "false");

        site.className = site.className.replace(" menu-toggled", "");
        document.body.className = document.body.className.replace(
            " menu-toggled--body",
            ""
        );
    };

    // Close small menu when user clicks outside
    document.addEventListener("click", function(event) {
        var isClickInside = container.contains(event.target);

        if (!isClickInside) {
            container.className = container.className.replace(" toggled", "");
            button.setAttribute("aria-expanded", "false");
            site.className = site.className.replace(" menu-toggled", "");
        }
    });

    // Get all the link elements within the menu.
    links = menu.getElementsByTagName("a");

    // Each time a menu link is focused or blurred, toggle focus.
    for (i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener("focus", toggleFocus, true);
        links[i].addEventListener("blur", toggleFocus, true);
    }

    /**
     * Sets or removes .focus class on an element.
     */
    function toggleFocus() {
        var self = this;

        // Move up through the ancestors of the current link until we hit .nav-menu.
        while (-1 === self.className.indexOf("nav-menu")) {
            // On li elements toggle the class .focus.
            if ("li" === self.tagName.toLowerCase()) {
                if (-1 !== self.className.indexOf("focus")) {
                    self.className = self.className.replace(" focus", "");
                } else {
                    self.className += " focus";
                }
            }

            self = self.parentElement;
        }
    }

    /**
     * Toggles `focus` class to allow submenu access on tablets.
     */
    (function() {
        var touchStartFn,
            parentLink = container.querySelectorAll(
                ".menu-item-has-children > a, .page_item_has_children > a"
            );

        if ("ontouchstart" in window) {
            touchStartFn = function(e) {
                var menuItem = this.parentNode;

                if (!menuItem.classList.contains("focus")) {
                    e.preventDefault();
                    for (i = 0; i < menuItem.parentNode.children.length; ++i) {
                        if (menuItem === menuItem.parentNode.children[i]) {
                            continue;
                        }
                        menuItem.parentNode.children[i].classList.remove(
                            "focus"
                        );
                    }
                    menuItem.classList.add("focus");
                } else {
                    menuItem.classList.remove("focus");
                }
            };

            for (i = 0; i < parentLink.length; ++i) {
                parentLink[i].addEventListener(
                    "touchstart",
                    touchStartFn,
                    false
                );
            }
        }
    })(container);
})();
