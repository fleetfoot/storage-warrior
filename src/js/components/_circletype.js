import CircleType from "circletype";

(function(CircleType) {
    const curvedElements = document.querySelectorAll(".sw-header__eyebrow");

    curvedElements.forEach(element => {
        const circleType = new CircleType(element);
        circleType.radius(600);
    });
})(CircleType);
