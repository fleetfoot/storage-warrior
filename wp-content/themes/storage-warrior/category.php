<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Storage_Warrior
 */

$blogid = get_option( 'page_for_posts' );

$display_title = get_field( 'page_blog_display_title', $blogid );
$large_title   = get_field( 'page_blog_title', $blogid );

$blog_placeholder_image = get_field( 'blog_placeholder_image', 55 );

get_header();

?>

<main id="primary" class="site-main">
    <header class="sw-section sw-header">
        <div class="sw-container">
            <h1 class="sw-header__eyebrow"><?php echo esc_html( $display_title ); ?></h1>
            <h2 class="sw-header__title"><?php single_cat_title(); ?></h2>
        </div>
    </header>

    <section class="sw-section">
        <div class="sw-container">
            <?php
            if ( have_posts() ) :
                ?>
                <div class="blog-archive">
                <?php
                while ( have_posts() ) :
                    the_post();
                    ?>

                    <a class="blog-archive__item" href="<?php the_permalink(); ?>">
                        <div class="blog-archive__image">
                            <?php
                            if ( has_post_thumbnail() ) :
                                the_post_thumbnail();
                            else :
                                echo wp_get_attachment_image(
                                    $blog_placeholder_image,
                                    'large'
                                );
                            endif;
                            ?>
                        </div>

                        <h3 class="blog-archive__title font-bold-md">
                            <?php the_title(); ?>
                        </h3>

                        <p class="blog-archive__read-more font-bold-xs uppercase-title uppercase-title--has-arrow">
                            Read more <?php storage_warrior_svg( 'long-arrow-right' ); ?>
                        </p>
                    </a>

                    <?php
                endwhile;
                ?>
                </div>
                <?php


            endif;
            ?>
        </div>
    </section>

    <?php
    the_posts_navigation(
        [
            'prev_text' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" aria-hidden="true"><path d="M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z"/></svg>' .
            esc_html__( 'Older posts', 'storage-warrior' ),
            'next_text' => esc_html__( 'Newer posts', 'storage-warrior' ) .
            '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" aria-hidden="true"><path d="M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z"/></svg>',
        ]
    );
    ?>

    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>
</main><!-- #main -->

<?php
get_footer();
