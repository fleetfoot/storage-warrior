<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Storage_Warrior
 */

$facebook_url = get_field( 'social_media_facebook_url', 55 );
$ig_url       = get_field( 'social_media_ig_url', 55 );

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <?php
    if ( is_front_page() || is_page_template( 'templates/ebay.php' ) ) :
        get_template_part( 'template-parts/header-script' );
    endif;
    ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'storage-warrior' ); ?></a>

    <header id="masthead" class="site-header">
        <div class="site-branding">
            <?php
            the_custom_logo();
            if ( is_front_page() && is_home() ) :
                ?>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                <?php
            else :
                ?>
                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                <?php
            endif;
            $storage_warrior_description = get_bloginfo( 'description', 'display' );
            if ( $storage_warrior_description || is_customize_preview() ) :
                ?>
                <p class="site-description"><?php echo $storage_warrior_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
            <?php endif; ?>
        </div><!-- .site-branding -->

        <nav id="site-navigation" class="main-navigation">
            <button id="hamburger-open" class="menu-toggle hamburger sw-button--no-style" aria-controls="primary-menu" aria-expanded="false" aria-label="Open Menu">
                <?php storage_warrior_hamburger(); ?>
            </button>

            <div class="hamburger-menu">

                <button id="hamburger-close" class="hamburger hamburger-close hidden sw-button--no-style" aria-label="Close Menu">
                    <?php storage_warrior_hamburger_close(); ?>
                </button>

                <div class="hamburger-menu__inner">


                    <div class="hamburger-menu__logo">
                        <?php echo wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'large' ); ?>
                    </div>

                    <?php
                    wp_nav_menu(
                        [
                            'theme_location' => 'main-menu',
                            'menu_id'        => 'primary-menu',
                            'menu_class'     => 'primary-menu',
                        ]
                    );
                    ?>

                    <div class="header-button">
                        <?php
                        $header_button = get_field( 'header_button', 55 );
                        ?>
                        <a href="<?php echo esc_url( $header_button['url'] ); ?>" class="sw-button sw-button--transparent uppercase-title">
                            <?php echo esc_html( $header_button['title'] ); ?>
                        </a>
                    </div>

                    <div class="header-social">
                        <a class="header-social__icon" target="_blank" href="<?php echo esc_url( $facebook_url ); ?>" aria-label="Facebook" rel="noopener"><?php storage_warrior_svg( 'facebook' ); ?></a>
                        <a class="header-social__icon" target="_blank" href="<?php echo esc_url( $ig_url ); ?>" aria-label="Instagram" rel="noopener"><?php storage_warrior_svg( 'instagram' ); ?></a>
                    </div>
                </div>
            </div>
        </nav><!-- #site-navigation -->


    </header><!-- #masthead -->
