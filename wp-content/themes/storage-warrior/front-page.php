<?php
/**
 * Front Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Storage_Warrior
 */

get_header();
?>

<main id="primary" class="site-main">
    <header class="sw-section sw-section--zero-margin">
        <?php get_template_part( 'template-parts/slider' ); ?>
    </header>

    <?php get_template_part( 'template-parts/content-with-image' ); ?>
    <?php get_template_part( 'template-parts/ebay-small' ); ?>
    <?php get_template_part( 'template-parts/internal-ctas' ); ?>
    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>


</main><!-- #main -->

<?php
get_footer();
