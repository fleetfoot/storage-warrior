<script type="text/javascript">
    function auction_nudge_loaded() {
        var controls = document.getElementById('an-controls-top');
        if (controls) {
            var search = controls.querySelector('#an-search-box');
            var submit = controls.querySelector('#an-search-submit');
            var select = controls.querySelector('#an-cats-nav').children[0];

            select.innerHTML = "Category";

            search.innerHTML = "Search";
            submit.innerHTML = "<svg class=\"search-icon\" xmlns=\"http://www.w3.org/2000/svg\" width=\"15\" height=\"15\" viewBox=\"0 0 24 24\"><path d=\"M23.822 20.88l-6.353-6.354c.93-1.465 1.467-3.2 1.467-5.059.001-5.219-4.247-9.467-9.468-9.467s-9.468 4.248-9.468 9.468c0 5.221 4.247 9.469 9.468 9.469 1.768 0 3.421-.487 4.839-1.333l6.396 6.396 3.119-3.12zm-20.294-11.412c0-3.273 2.665-5.938 5.939-5.938 3.275 0 5.94 2.664 5.94 5.938 0 3.275-2.665 5.939-5.94 5.939-3.274 0-5.939-2.664-5.939-5.939z\"/></svg>";

            search.addEventListener('focus', function(e) {
                if (search.innerHTML == 'Search') {
                    search.innerHTML = '';
                }
            });
        }

        var pagination = document.getElementById('an-page-bot');
        if (pagination) {
            pagination.querySelector('#an-page-bot-prev').innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"16\" viewBox=\"0 0 24 24\"><path fill=\"9c9c9c\" d=\"M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z\"/></svg> Previous"

            pagination.querySelector('#an-page-bot-next').innerHTML = "Next <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"16\" viewBox=\"0 0 24 24\"><path fill=\"9c9c9c\" d=\"M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z\"/></svg>"
        }

        var anItems = document.getElementById('an-item-wrap');
        var itemArray = anItems.getElementsByClassName("an-item");

        for (var i = 0; i < itemArray.length; i++) {
            var title = itemArray[i].querySelector(".an-title");
            var image = itemArray[i].querySelector(".an-image");
            image.insertAdjacentElement('afterend', title);
            // title.remove();
            itemArray[i].querySelector('.an-view > a').innerHTML = "Buy it now <svg class=\"sw-arrow\" viewBox=\"0 0 27 28\" preserveAspectRatio=\"xMidYMid meet\">\
        <path d=\"M27 13.953c0 0.141-0.063 0.281-0.156 0.375l-6 5.531c-0.156 0.141-0.359 0.172-0.547 0.094-0.172-0.078-0.297-0.25-0.297-0.453v-3.5h-19.5c-0.281 0-0.5-0.219-0.5-0.5v-3c0-0.281 0.219-0.5 0.5-0.5h19.5v-3.5c0-0.203 0.109-0.375 0.297-0.453s0.391-0.047 0.547 0.078l6 5.469c0.094 0.094 0.156 0.219 0.156 0.359v0z\"></path>\
        </svg>";
        }
    }
</script>
