<?php
/**
 * Template part - eBay display 4 items
 *
 * @package Storage_Warrior
 */

$ebay_title = get_field( 'home_ebay_title' );
$shortcode  = get_field( 'home_ebay_an' );
$ebay_link  = get_field( 'home_ebay_link' );
?>

<div class="sw-section">
    <div class="sw-container ebay-small">
        <h3 class="h4 ebay-small__title"><?php echo esc_html( $ebay_title ); ?></h3>
        <div class="ebay-grid">
            <?php echo do_shortcode( $shortcode ); ?>
        </div>
        <a class="sw-button sw-button--large ebay-small__button" href="<?php echo esc_html( $ebay_link['url'] ); ?>">
            <?php echo esc_html( $ebay_link['title'] ); ?>
        </a>
    </div>
</div>
