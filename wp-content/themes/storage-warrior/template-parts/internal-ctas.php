<?php
/**
 * Template part - Internal CTAs
 *
 * @package Storage_Warrior
 */

$cta_1 = get_field( 'int_cta_1', 55 );
$cta_2 = get_field( 'int_cta_2', 55 );
$cta_3 = get_field( 'int_cta_3', 55 );

$ctas = [ $cta_1, $cta_2, $cta_3 ];
?>

<section class="sw-section">
    <div class="sw-container int-ctas">
        <?php
        foreach ( $ctas as $cta ) :
            ?>
            <a href="<?php echo esc_url( $cta['link']['url'] ); ?>" class="int-ctas__cta">
                <h5 class="int-ctas__title font-heading-sm"><?php echo esc_html( $cta['title'] ); ?></h5>
                <p class="int-ctas__caption"><?php echo esc_html( $cta['caption'] ); ?></p>

                <p class="int-ctas__link uppercase-title uppercase-title--has-arrow">

                    <?php echo esc_html( $cta['link']['title'] ); ?>

                    <span class="sw-arrow__wrapper">
                        <?php storage_warrior_svg( 'long-arrow-right', 'black' ); ?>
                    </span>
                </p>
            </a>
        <?php endforeach; ?>
   </div>
</section>
