<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Storage_Warrior
 */

$categories             = get_the_category();
$blog_placeholder_image = get_field( 'blog_placeholder_image', 55 );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="sw-section entry-header blog-header">
        <div class="sw-container blog-header__container">
            <?php if ( is_singular() ) : ?>
                <h2 class="blog-header__eyebrow sw-header__eyebrow"><?php echo __( 'Storage Warrior Blog' ); ?></h2>
            <?php endif; ?>

            <div class="blog-header__main">
                <div class="blog-header__image">
                    <?php
                    if ( has_post_thumbnail() ) :
                        the_post_thumbnail( 'large', [ 'loading' => 'lazy' ] );
                    else :
                        echo wp_get_attachment_image(
                            $blog_placeholder_image,
                            'large',
                            false,
                            [
                                'loading' => 'lazy',
                            ]
                        );
                    endif;
                    ?>
                </div>

                <div class="blog-header__content">
                    <?php
                    if ( $categories && 'uncategorized' !== $categories[0]->slug ) :
                        ?>
                        <h3 class="blog-header__category">
                            <?php echo esc_html( $categories[0]->name ); ?>
                        </h3>
                    <?php endif; ?>

                    <?php
                    the_title( '<h1 class="blog-header__title h2">', '</h1>' );
                    ?>

                    <div class="blog-header__social-media">
                        <?php get_template_part( 'template-parts/social-media' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- .entry-header -->

    <section class="sw-section">
        <div class="sw-container">
            <div class="sw-content">
                <?php
                if ( is_singular() ) :
                    the_content(
                        sprintf(
                            wp_kses(
                            /* translators: %s: Name of current post. Only visible to screen readers */
                                __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'storage-warrior' ),
                                [
                                    'span' => [
                                        'class' => [],
                                    ],
                                ]
                            ),
                            wp_kses_post( get_the_title() )
                        )
                    );
                    endif;
                ?>
            </div>
        </div><!-- .entry-content -->
    </section>
</article><!-- #post-<?php the_ID(); ?> -->
