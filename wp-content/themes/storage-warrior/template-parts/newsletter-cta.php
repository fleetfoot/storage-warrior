<?php
/**
 * Template part - Newsletter CTA
 *
 * @package Storage_Warrior
 */

$news_title = get_field( 'news_cta_title', 55 );
$shortcode  = get_field( 'news_cta_mc_shortcode', 55 );
?>

<section class="sw-section">
    <div class="sw-container news-cta">
        <h5 class="news-cta__title font-heading-xs"><?php echo esc_html( $news_title ); ?></h5>
        <?php echo do_shortcode( '[mc4wp_form id="454"]' ); ?>
    </div>
</section>
