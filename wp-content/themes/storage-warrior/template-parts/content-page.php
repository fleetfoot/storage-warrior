<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Storage_Warrior
 */

$eyebrow = get_field( 'post_eyebrow' );
?>

<div class="sw-section">
    <div class="sw-container">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <header class="sw-section sw-header">
            <div class="sw-container">
                <?php if ( $eyebrow ) : ?>
                    <h1 class="sw-header__eyebrow"><?php echo esc_html( $eyebrow ); ?></h1>
                    <h2 class="sw-header__title"><?php the_title(); ?></h2>
                <?php else : ?>
                    <?php the_title( '<h1 class="entry-title sw-header__title">', '</h1>' ); ?>
                <?php endif; ?>
            </div>
        </header>

        <?php storage_warrior_post_thumbnail(); ?>

        <div class="entry-content">
            <?php
            the_content();

            wp_link_pages(
                [
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'storage-warrior' ),
                    'after'  => '</div>',
                ]
            );
            ?>
        </div><!-- .entry-content -->

        <?php if ( get_edit_post_link() ) : ?>
            <footer class="entry-footer">
                <?php
                edit_post_link(
                    sprintf(
                        wp_kses(
                            /* translators: %s: Name of current post. Only visible to screen readers */
                            __( 'Edit <span class="screen-reader-text">%s</span>', 'storage-warrior' ),
                            [
                                'span' => [
                                    'class' => [],
                                ],
                            ]
                        ),
                        wp_kses_post( get_the_title() )
                    ),
                    '<span class="edit-link">',
                    '</span>'
                );
                ?>
            </footer><!-- .entry-footer -->
        <?php endif; ?>
        </article><!-- #post-<?php the_ID(); ?> -->
    </div>
</div>

