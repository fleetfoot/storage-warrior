<?php
/**
 * Template part - Content with Image
 *
 * @package Storage_Warrior
 */

$section_title = get_field( 'home_content_title' );
$text          = get_field( 'home_content_text' );
$section_link  = get_field( 'home_content_link' );
$image_id      = get_field( 'home_content_image' );

?>

<section class="sw-section">
    <div class="sw-container content-wimage">
        <div class="content-wimage__content">
            <h4 class="content-wimage__title font-heading-md"><?php echo esc_html( $section_title ); ?></h4>
            <p class="content-wimage__text"><?php echo wp_kses_post( $text ); ?></p>
            <a class="content-wimage__button font-bold-lg sw-button sw-button--transparent"
              href="<?php echo esc_url( $section_link['url'] ); ?>">
                <?php echo esc_html( $section_link['title'] ); ?>
            </a>
        </div>

        <div class="content-wimage__image-wrapper">
            <?php
            echo wp_get_attachment_image(
                $image_id,
                'large',
                false,
                [
                    'class'   => 'content-wimage__image',
                    'loading' => 'lazy',
                ]
            );
            ?>
        </div>
   </div>
</section>

