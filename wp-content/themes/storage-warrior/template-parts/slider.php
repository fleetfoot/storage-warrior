<?php
/**
 * Template part - Slider
 *
 * @package Storage_Warrior
 */

$slides = get_field( 'slides' );

?>

<div class="sw-container sw-container">
    <div id="sw-slider" class="sw-slider">
        <?php
        /* iterate over 3 slides */
        for ( $i = 1; $i <= 3; $i++ ) :
            if ( $slides[ 'image_' . $i ] ) :
                ?>
            <div class="sw-slider__slide" loading="lazy"
                style="background-image: url( <?php echo esc_url( $slides[ 'image_' . $i ] ); ?> )">
                <div class="sw-slider__content">
                    <h3 class="h4 uppercase sw-slider__eyebrow sw-header__eyebrow">
                        <?php echo esc_html( $slides[ 'eyebrow_' . $i ] ); ?>
                    </h3>

                    <h2 class="h1 sw-slider__title align-center">
                        <?php echo wp_kses_post( $slides[ 'title_' . $i ] ); ?>
                    </h2>

                    <p class="font-heading-lg sw-slider__subtitle align-center">
                        <?php echo wp_kses_post( $slides[ 'subtitle_' . $i ] ); ?>
                    </p>

                    <a class="sw-button sw-slider__button"
                        href="<?php echo esc_url( $slides[ 'button_' . $i ]['url'] ); ?>">
                        <?php echo esc_html( $slides[ 'button_' . $i ]['title'] ); ?>
                        <?php storage_warrior_svg( 'long-arrow-right', 'large' ); ?>
                    </a>
                </div>
            </div>
            <?php endif; ?>
        <?php endfor; ?>
    </div>
</div>
