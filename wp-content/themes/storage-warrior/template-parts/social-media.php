<?php
/**
 * Template part - Social Media
 *
 * @package Storage_Warrior
 */

$facebook_url = get_field( 'social_media_facebook_url', 55 );
$twitter_url  = get_field( 'social_media_twitter_url', 55 );
$reddit_url   = get_field( 'social_media_reddit_url', 55 );
$email        = get_field( 'sw_email', 55 );
?>

<div class="social-share">
    <?php if ( $facebook_url ) : ?>
        <a class="social-share__icon"
           aria-label="Facebook"
           href="<?php echo esc_url( $facebook_url ); ?>"
           rel="noopener">
            <?php storage_warrior_svg( 'facebook' ); ?>
        </a>
    <?php endif; ?>

    <?php if ( $twitter_url ) : ?>
        <a class="social-share__icon"
            aria-label="Twitter"
           href="<?php echo esc_url( $twitter_url ); ?>"
           rel="noopener">
            <?php storage_warrior_svg( 'twitter' ); ?>
        </a>
    <?php endif; ?>

    <?php if ( $reddit_url ) : ?>
        <a class="social-share__icon"
        aria-label="Reddit"
           href="<?php echo esc_url( $reddit_url ); ?>"
           rel="noopener">
            <?php storage_warrior_svg( 'reddit' ); ?>
        </a>
    <?php endif; ?>

    <?php if ( $email ) : ?>
        <a class="social-share__icon"
           aria-label="Email"
           href="mailto:<?php echo esc_url( $email ); ?>"
           rel="noopener">
            <?php storage_warrior_svg( 'email' ); ?>
        </a>
    <?php endif; ?>
</div>
