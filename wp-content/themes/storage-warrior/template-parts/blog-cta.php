<?php
/**
 * Template part - Blog CTA
 *
 * @package Storage_Warrior
 */

$cta_text = get_field( 'blog_cta_text', 55 );
$cta_link = get_field( 'blog_cta_link', 55 );
?>

<section class="sw-section">
    <div class="sw-container sw-container--small">
        <div class="blog-cta">
            <p class="blog-cta__text"><?php echo esc_html( $cta_text ); ?></p>
            <a class="sw-button sw-button--large blog-cta__button"
               href="<?php echo esc_url( $cta_link['url'] ); ?>">
                <?php echo esc_html( $cta_link['title'] ); ?>
            </a>
        </div>
    </div>
</section>
