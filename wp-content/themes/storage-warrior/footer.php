<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Storage_Warrior
 */

$tos = get_page_link( 87 );
?>

    <footer id="colophon" class="site-footer sw-section">
        <div class="sw-container sw-footer">
            <p class="sw-footer__copyright">
                &copy; Storage Warrior <?php echo date( 'Y' ); ?>. All rights reserved.
                <span class="sw-footer__divider"> | </span>
                <a class="sw-footer__copyright__link" href="<?php echo esc_url( $tos ); ?>">Terms of Service</a>
            </p>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
