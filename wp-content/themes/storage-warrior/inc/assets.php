<?php
/**
 * Enqueue scripts and styles.
 *
 * @package Storage Warrior
 */

/**
 * Scripts and styles.
 *
 * @package Storage Warrior
 */
function storage_warrior_scripts() {
    $my_js_ver  = gmdate( 'ymd-Gis', filemtime( plugin_dir_path( __FILE__ ) . 'dist/js/index.js' ) );
    $my_css_ver = gmdate( 'ymd-Gis', filemtime( plugin_dir_path( __FILE__ ) . 'dist/css/index.css' ) );

    /* Add Slick slider only to front page */
    if ( is_front_page() ) {
        wp_enqueue_style( 'slick-slider-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', false, '1.8.1' );
        wp_enqueue_script( 'slick-slider-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', [ 'jquery' ], '1.8.1', true );
    }

    /* Storage Warrior Stylesheet */
    wp_enqueue_style( 'storage-warrior-style', get_template_directory_uri() . '/dist/css/index.css', [], $my_css_ver );
    wp_style_add_data( 'storage-warrior-style', 'rtl', 'replace' );

    /* Google fonts */
    wp_enqueue_style(
        'google-fonts',
        'https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,700;1,400&display=swap',
        false,
        $my_css_ver
    );

    /* Storage Warrior Scripts */
    wp_enqueue_script( 'storage-warrior-scripts', get_template_directory_uri() . '/dist/js/index.js', [ 'jquery', 'customize-preview' ], $my_js_ver, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'storage_warrior_scripts' );
