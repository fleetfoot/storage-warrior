<?php
/**
 * Disable Editor
 *
 * @package      Storage Warrior
 * @author       Bill Erickson
 * @since        1.0.0
 * @license      GPL-2.0+
 **/

/**
 * Templates and Page IDs without editor
 *
 *  @param bool $id boolean.
 */
function storage_warrior_disable_editor( $id = false ) {

    $excluded_templates = [
        'templates/modules.php',
        'templates/contact.php',
        'templates/about.php',
        'templates/estate-services.php',
        'templates/ebay.php',
    ];

    $excluded_ids = [
        get_option( 'page_on_front' ),
        '55', /* Theme Options Page */
    ];

    if ( empty( $id ) ) {
        return false;
    }

    $id       = intval( $id );
    $template = get_page_template_slug( $id );

    return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
}

/**
 * Disable Gutenberg by template
 *
 *  @param bool   $can_edit boolean.
 *  @param string $post_type boolean.
 */
function storage_warrior_disable_gutenberg( $can_edit, $post_type ) {

    if ( ! ( is_admin() && ! empty( $_GET['post'] ) ) ) {
        return $can_edit;
    }

    if ( storage_warrior_disable_editor( $_GET['post'] ) ) {
        $can_edit = false;
    }

    return $can_edit;

}
add_filter( 'gutenberg_can_edit_post_type', 'storage_warrior_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'storage_warrior_disable_gutenberg', 10, 2 );

/**
 * Disable Classic Editor by template
 */
function storage_warrior_disable_classic_editor() {

    $screen = get_current_screen();
    if ( 'page' !== $screen->id || ! isset( $_GET['post'] ) ) {
        return;
    }

    if ( storage_warrior_disable_editor( $_GET['post'] ) ) {
        remove_post_type_support( 'page', 'editor' );
    }

}
add_action( 'admin_head', 'storage_warrior_disable_classic_editor' );

/**
 * Social Media Functions from IcoMoon & Iconmonstr
 * https://icomoon.io/app/#/select
 * https://iconmonstr.com/
 *
 * @package Storage Warrior
 *
 * @param string $type social media icon.
 * @param string $class class names.
 */
function storage_warrior_svg( $type, $class = null ) {
    switch ( $type ) {
        case 'facebook':
            echo '<svg class="icon icon-facebook" viewBox="0 0 32 32">
            <path d="M19 6h5v-6h-5c-3.86 0-7 3.14-7 7v3h-4v6h4v16h6v-16h5l1-6h-6v-3c0-0.542 0.458-1 1-1z"></path>
            </svg>';
            break;
        case 'twitter':
            echo '<svg class="icon icon-twitter" viewBox="0 0 32 32">
            <path d="M32 7.075c-1.175 0.525-2.444 0.875-3.769 1.031 1.356-0.813 2.394-2.1 2.887-3.631-1.269 0.75-2.675 1.3-4.169 1.594-1.2-1.275-2.906-2.069-4.794-2.069-3.625 0-6.563 2.938-6.563 6.563 0 0.512 0.056 1.012 0.169 1.494-5.456-0.275-10.294-2.888-13.531-6.862-0.563 0.969-0.887 2.1-0.887 3.3 0 2.275 1.156 4.287 2.919 5.463-1.075-0.031-2.087-0.331-2.975-0.819 0 0.025 0 0.056 0 0.081 0 3.181 2.263 5.838 5.269 6.437-0.55 0.15-1.131 0.231-1.731 0.231-0.425 0-0.831-0.044-1.237-0.119 0.838 2.606 3.263 4.506 6.131 4.563-2.25 1.762-5.075 2.813-8.156 2.813-0.531 0-1.050-0.031-1.569-0.094 2.913 1.869 6.362 2.95 10.069 2.95 12.075 0 18.681-10.006 18.681-18.681 0-0.287-0.006-0.569-0.019-0.85 1.281-0.919 2.394-2.075 3.275-3.394z"></path>
            </svg>';
            break;
        case 'linkedin':
            echo '<svg class="icon icon-linkedin2" viewBox="0 0 32 32">
            <path d="M12 12h5.535v2.837h0.079c0.77-1.381 2.655-2.837 5.464-2.837 5.842 0 6.922 3.637 6.922 8.367v9.633h-5.769v-8.54c0-2.037-0.042-4.657-3.001-4.657-3.005 0-3.463 2.218-3.463 4.509v8.688h-5.767v-18z"></path>
            <path d="M2 12h6v18h-6v-18z"></path>
            <path d="M8 7c0 1.657-1.343 3-3 3s-3-1.343-3-3c0-1.657 1.343-3 3-3s3 1.343 3 3z"></path>
            </svg>';
            break;
        case 'instagram':
            echo '<svg id="icon-instagram" viewBox="0 0 32 32">
            <path d="M16 2.881c4.275 0 4.781 0.019 6.462 0.094 1.563 0.069 2.406 0.331 2.969 0.55 0.744 0.288 1.281 0.638 1.837 1.194 0.563 0.563 0.906 1.094 1.2 1.838 0.219 0.563 0.481 1.412 0.55 2.969 0.075 1.688 0.094 2.194 0.094 6.463s-0.019 4.781-0.094 6.463c-0.069 1.563-0.331 2.406-0.55 2.969-0.288 0.744-0.637 1.281-1.194 1.837-0.563 0.563-1.094 0.906-1.837 1.2-0.563 0.219-1.413 0.481-2.969 0.55-1.688 0.075-2.194 0.094-6.463 0.094s-4.781-0.019-6.463-0.094c-1.563-0.069-2.406-0.331-2.969-0.55-0.744-0.288-1.281-0.637-1.838-1.194-0.563-0.563-0.906-1.094-1.2-1.837-0.219-0.563-0.481-1.413-0.55-2.969-0.075-1.688-0.094-2.194-0.094-6.463s0.019-4.781 0.094-6.463c0.069-1.563 0.331-2.406 0.55-2.969 0.288-0.744 0.638-1.281 1.194-1.838 0.563-0.563 1.094-0.906 1.838-1.2 0.563-0.219 1.412-0.481 2.969-0.55 1.681-0.075 2.188-0.094 6.463-0.094zM16 0c-4.344 0-4.887 0.019-6.594 0.094-1.7 0.075-2.869 0.35-3.881 0.744-1.056 0.412-1.95 0.956-2.837 1.85-0.894 0.888-1.438 1.781-1.85 2.831-0.394 1.019-0.669 2.181-0.744 3.881-0.075 1.713-0.094 2.256-0.094 6.6s0.019 4.887 0.094 6.594c0.075 1.7 0.35 2.869 0.744 3.881 0.413 1.056 0.956 1.95 1.85 2.837 0.887 0.887 1.781 1.438 2.831 1.844 1.019 0.394 2.181 0.669 3.881 0.744 1.706 0.075 2.25 0.094 6.594 0.094s4.888-0.019 6.594-0.094c1.7-0.075 2.869-0.35 3.881-0.744 1.050-0.406 1.944-0.956 2.831-1.844s1.438-1.781 1.844-2.831c0.394-1.019 0.669-2.181 0.744-3.881 0.075-1.706 0.094-2.25 0.094-6.594s-0.019-4.887-0.094-6.594c-0.075-1.7-0.35-2.869-0.744-3.881-0.394-1.063-0.938-1.956-1.831-2.844-0.887-0.887-1.781-1.438-2.831-1.844-1.019-0.394-2.181-0.669-3.881-0.744-1.712-0.081-2.256-0.1-6.6-0.1v0z"></path>
            <path d="M16 7.781c-4.537 0-8.219 3.681-8.219 8.219s3.681 8.219 8.219 8.219 8.219-3.681 8.219-8.219c0-4.537-3.681-8.219-8.219-8.219zM16 21.331c-2.944 0-5.331-2.387-5.331-5.331s2.387-5.331 5.331-5.331c2.944 0 5.331 2.387 5.331 5.331s-2.387 5.331-5.331 5.331z"></path>
            <path d="M26.462 7.456c0 1.060-0.859 1.919-1.919 1.919s-1.919-0.859-1.919-1.919c0-1.060 0.859-1.919 1.919-1.919s1.919 0.859 1.919 1.919z"></path>
            </svg>';
            break;
        case 'long-arrow-right':
            echo '<svg aria-hidden="true" class="sw-arrow sw-arrow--' . esc_html( $class ) . '" viewBox="0 0 27 28" preserveAspectRatio="xMidYMid meet">
            <path d="M27 13.953c0 0.141-0.063 0.281-0.156 0.375l-6 5.531c-0.156 0.141-0.359 0.172-0.547 0.094-0.172-0.078-0.297-0.25-0.297-0.453v-3.5h-19.5c-0.281 0-0.5-0.219-0.5-0.5v-3c0-0.281 0.219-0.5 0.5-0.5h19.5v-3.5c0-0.203 0.109-0.375 0.297-0.453s0.391-0.047 0.547 0.078l6 5.469c0.094 0.094 0.156 0.219 0.156 0.359v0z"></path>
            </svg>';
            break;
        case 'reddit':
            echo '<svg xmlns="http://www.w3.org/2000/svg"  width="24" height="24" viewBox="0 0 24 24">
            <path d="M24 11.779c0-1.459-1.192-2.645-2.657-2.645-.715 0-1.363.286-1.84.746-1.81-1.191-4.259-1.949-6.971-2.046l1.483-4.669 4.016.941-.006.058c0 1.193.975 2.163 2.174 2.163 1.198 0 2.172-.97 2.172-2.163s-.975-2.164-2.172-2.164c-.92 0-1.704.574-2.021 1.379l-4.329-1.015c-.189-.046-.381.063-.44.249l-1.654 5.207c-2.838.034-5.409.798-7.3 2.025-.474-.438-1.103-.712-1.799-.712-1.465 0-2.656 1.187-2.656 2.646 0 .97.533 1.811 1.317 2.271-.052.282-.086.567-.086.857 0 3.911 4.808 7.093 10.719 7.093s10.72-3.182 10.72-7.093c0-.274-.029-.544-.075-.81.832-.447 1.405-1.312 1.405-2.318zm-17.224 1.816c0-.868.71-1.575 1.582-1.575.872 0 1.581.707 1.581 1.575s-.709 1.574-1.581 1.574-1.582-.706-1.582-1.574zm9.061 4.669c-.797.793-2.048 1.179-3.824 1.179l-.013-.003-.013.003c-1.777 0-3.028-.386-3.824-1.179-.145-.144-.145-.379 0-.523.145-.145.381-.145.526 0 .65.647 1.729.961 3.298.961l.013.003.013-.003c1.569 0 2.648-.315 3.298-.962.145-.145.381-.144.526 0 .145.145.145.379 0 .524zm-.189-3.095c-.872 0-1.581-.706-1.581-1.574 0-.868.709-1.575 1.581-1.575s1.581.707 1.581 1.575-.709 1.574-1.581 1.574z"/></svg>
            ';
            break;
        case 'email':
            echo '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12.713l-11.985-9.713h23.97l-11.985 9.713zm0 2.574l-12-9.725v15.438h24v-15.438l-12 9.725z"/></svg>';
            break;
        case 'arrow':
            echo '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z"/></svg>';
            break;
    }
}

/**
 * Hamburger Menu SVG
 *
 * @package Storage Warrior.
 */
function storage_warrior_hamburger() {
    echo '<svg class="hamburger__icon" viewBox="0 0 24 16" width="24" height="16" xmlns="http://www.w3.org/2000/svg">
<rect width="24" height="2"></rect>
    <rect width="24" height="2" y="7"></rect>
    <rect width="24" height="2" y="14"></rect>
    </svg>';
}

/**
 * Hamburger Close SVG
 *
 * @package Storage Warrior.
 */
function storage_warrior_hamburger_close() {
    echo '<svg class="hamburger-close__icon" viewBox="0 0 16 16" fill="#4a4a4a" width="20" height="16" xmlns="http://www.w3.org/2000/svg">
<path d="M1.414213562373095 0 16 14.585786437626904 L14.585786437626904 16 L0 1.414213562373095"></path>
<path d="M14.585786437626904 0 L16 1.414213562373095 L1.414213562373095 16 L0 14.585786437626904"></path>
</svg>';
}

/**
 * Register a custom menu page.
 */
function storage_warrior_custom_menu_page() {
    add_menu_page(
        __( 'Storage Warrior Theme Options', 'textdomain' ),
        'Storage Warrior Options',
        'manage_options',
        'post.php?post=55&action=edit',
        '',
        'dashicons-admin-generic',
        31
    );
}
add_action( 'admin_menu', 'storage_warrior_custom_menu_page' );

/**
 * Add styles to Gutenberg.
 */
function storage_warrior_editor_css() {
    add_theme_support( 'editor-styles' );
    add_editor_style( 'dist/css/editor-styles.css' );
}
add_action( 'after_setup_theme', 'storage_warrior_editor_css' );

/**
 * Add colors to Gutenberg.
 */
function storage_warrior_editor_colors() {
    add_theme_support(
        'editor-color-palette',
        [
            [
                'name'  => __( 'SW Orange', 'storageWarrior' ),
                'slug'  => 'sw-orange',
                'color' => '#f74733',
            ],
            [
                'name'  => __( 'SW Black', 'storageWarrior' ),
                'slug'  => 'sw-black',
                'color' => '#293647',
            ],
            [
                'name'  => __( 'SW Blue', 'storageWarrior' ),
                'slug'  => 'sw-blue',
                'color' => '#006f92',
            ],
            [
                'name'  => __( 'SW Green', 'storageWarrior' ),
                'slug'  => 'sw-green',
                'color' => '#65ab9c',
            ],
            [
                'name'  => __( 'SW Yellow', 'storageWarrior' ),
                'slug'  => 'sw-yellow',
                'color' => '#ffa659',
            ],
            [
                'name'  => __( 'SW Grey', 'storageWarrior' ),
                'slug'  => 'sw-grey',
                'color' => '#9c9c9c',
            ],
            [
                'name'  => __( 'SW Dark Orange', 'storageWarrior' ),
                'slug'  => 'sw-dark-orange',
                'color' => '#b81300',
            ],
            [
                'name'  => __( 'SW Background', 'storageWarrior' ),
                'slug'  => 'sw-background',
                'color' => '#f5f5f5',
            ],
        ]
    );
}
add_action( 'after_setup_theme', 'storage_warrior_editor_colors' );

/**
 * Removes some menus by page.
 */
function storage_warrior_remove_menu_pages() {

    remove_menu_page( 'edit-comments.php' );
    remove_submenu_page( 'themes.php', 'widgets.php' );
    remove_submenu_page( 'themes.php', 'themes.php' );

}
add_action( 'admin_menu', 'storage_warrior_remove_menu_pages' );

/**
 * Removes ACF menu from admin.
 */
add_filter( 'acf/settings/show_admin', '__return_false' );

/**
 * Login styles.
 */
function storage_warrior_login_styles() {
    $logo  = get_theme_mod( 'custom_logo' );
    $image = wp_get_attachment_image_src( $logo, 'full' );
    ?>
    <style type="text/css">
        body.login {
            background: #f5f5f5;
        }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo esc_url( $image[0] ); ?>);
            height:130px;
            width:320px;
            background-size: 320px 130px;
            background-repeat: no-repeat;
        }
        #login p, #login input {
            color: #293647;
        }
        #login a {
            color: #293647;
        }
        #login a:hover {
            color: #293647 !important;
        }
        #login input[type=submit] {
            color: #fff;
            background: #f74733;
            border-color: #f74733;
            font-weight: 600;
            transition: all 200ms;
        }
        #login input[type=submit]:hover {
            background: #b81300;
            border-color: #b81300;
        }
        #login #login_error, #login .message, #login .success {
            border-color: #293647;
        }
    </style>
    <?php
}
add_action( 'login_enqueue_scripts', 'storage_warrior_login_styles' );

/**
 * Login Page URL.
 */
function storage_warrior_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'storage_warrior_login_logo_url' );

/**
 * Login Page URL text.
 */
function storage_warrior_login_logo_url_title() {
    return 'Storage Warrior - Home';
}
add_filter( 'login_headertext', 'storage_warrior_login_logo_url_title' );
