<?php
/**
 * Front Page
 *
 * @package Storage Warrior
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Theme Settings
 */
Container::make( 'theme_options', __( 'Storage Warrior Options' ) )
->set_icon( 'dashicons-hammer' )
->add_tab(
    __( 'General' ),
    []
)
->add_tab(
    __( 'Internal CTAs' ),
    [
        Field::make( 'complex', 'theme_options_ctas', 'CTAs' )
            ->set_min( 3 )
            ->set_max( 3 )
            ->setup_labels(
                [
                    'plural_name'   => 'CTAs',
                    'singular_name' => 'CTA',
                ]
            )
            ->add_fields(
                'cta',
                [
                    Field::make( 'text', 'title', __( 'Title' ) ),
                    Field::make( 'text', 'caption', __( 'Caption' ) ),
                    Field::make( 'text', 'link-text', __( 'Link Text' ) )
                    ->set_width( 50 ),
                    Field::make( 'text', 'link', __( 'Link URL' ) )
                    ->set_width( 50 ),
                ]
            ),
    ]
)
->add_tab(
    __( 'Newsletter CTA' ),
    [
        Field::make( 'text', 'settings_newsletter_text', __( 'Text' ) ),
        Field::make( 'text', 'settings_newsletter_button', __( 'Button Text' ) )
        ->set_width( 50 ),
    ]
);
