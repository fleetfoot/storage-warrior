<?php
/**
 * Front Page
 *
 * @package Storage Warrior
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', __( 'Homepage' ) )
->where( 'post_id', '=', get_option( 'page_on_front' ) )
->add_tab(
    __( 'Slider' ),
    [
        Field::make( 'complex', 'homepage_slides', 'Slides' )
            ->set_layout( 'tabbed-horizontal' )
            ->set_required( true )
            ->set_max( 5 )
            ->setup_labels(
                [
                    'plural_name'   => 'Slides',
                    'singular_name' => 'Slide',
                ]
            )
            ->add_fields(
                [
                    Field::make( 'text', 'eyebrow', 'Eyebrow text' )
                        ->set_required( true ),
                    Field::make( 'text', 'title', 'Title' )
                        ->set_required( true ),
                    Field::make( 'text', 'subtitle', 'Subtitle' )
                        ->set_required( true ),
                    Field::make( 'image', 'image', 'Image' )
                        ->set_value_type( 'url' )
                        ->set_required( true ),
                    Field::make( 'text', 'button_text', 'Button text' )
                        ->set_width( 50 )
                        ->set_required( true ),
                    Field::make( 'text', 'button_link', 'Button link' )
                        ->set_width( 50 )
                        ->set_required( true ),
                ]
            ),
    ]
)
->add_tab(
    __( 'Content' ),
    [
        Field::make( 'text', 'home_content_title', __( 'Title' ) ),
        Field::make( 'textarea', 'home_content_text', __( 'Text' ) )
            ->set_rows( 4 ),
        Field::make( 'text', 'home_content_link_text', 'Link text' )
            ->set_width( 50 )
            ->set_required( true ),
        Field::make( 'text', 'home_content_link_url', 'Link URL' )
            ->set_width( 50 )
            ->set_required( true ),
        Field::make( 'image', 'home_content_image', 'Image' )
            ->set_required( true ),
    ]
);
