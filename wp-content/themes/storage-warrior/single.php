<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Storage_Warrior
 */

get_header();
?>

    <main id="primary" class="site-main">

        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', get_post_type() );
            get_template_part( 'template-parts/blog-cta' );

            the_post_navigation(
                [
                    'prev_text' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" aria-hidden="true"><path d="M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z"/></svg>' .
                        esc_html__( 'Previous post', 'storage-warrior' ),
                    'next_text' => esc_html__( 'Next post', 'storage-warrior' ) .
                        '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" aria-hidden="true"><path d="M12.068.016l-3.717 3.698 5.263 5.286h-13.614v6h13.614l-5.295 5.317 3.718 3.699 11.963-12.016z"/></svg>',
                ]
            );

            get_template_part( 'template-parts/newsletter-cta' );

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->

<?php
get_footer();
