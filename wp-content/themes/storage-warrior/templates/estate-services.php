<?php
/**
 * Template Name: Estate Services
 *
 * @package Storage_Warrior
 */

$es_title    = get_field( 'page_es_title' );
$es_subtitle = get_field( 'page_es_subtitle' );

$section_1 = get_field( 'page_es_section_1' );
$section_2 = get_field( 'page_es_section_2' );
$section_3 = get_field( 'page_es_section_3' );

$sections = [ $section_1, $section_2, $section_3 ];

$cta = get_field( 'page_es_cta' );

get_header();
?>

<main id="primary" class="site-main">
    <header class="sw-section sw-header">
        <div class="sw-container">
            <h1 class="sw-header__eyebrow"><?php the_title(); ?></h1>
            <h2 class="sw-header__title"><?php echo esc_html( $es_title ); ?></h2>
            <?php if ( $es_subtitle ) : ?>
                <div class="sw-header__subtitle">
                    <?php echo wp_kses_post( $es_subtitle ); ?>
                </div>
            <?php endif; ?>
        </div>
    </header>

    <section class="sw-section">
        <div class="sw-container">
            <?php foreach ( $sections as $section ) : ?>
                <?php if ( $section['image'] && $section['content'] ) : ?>
                <div class="sw-row">
                    <div class="sw-row__image">
                        <?php
                        echo wp_get_attachment_image(
                            $section['image'],
                            'large',
                            false,
                            [
                                'loading' => 'lazy',
                            ]
                        );
                        ?>
                    </div>

                    <div class="sw-row__content">
                        <?php echo wp_kses_post( $section['content'] ); ?>
                    </div>
                </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </section>

    <?php if ( $cta['link'] ) : ?>
        <section class="sw-section">
            <div class="sw-container">
                <div class="int-ctas int-ctas--single">
                    <a href="<?php echo esc_url( $cta['link']['url'] ); ?>" class="int-ctas__cta">
                        <h5 class="int-ctas__title font-heading-sm"><?php echo esc_html( $cta['title'] ); ?></h5>
                        <?php if ( $cta['text'] ) : ?>
                            <p class="int-ctas__caption"><?php echo esc_html( $cta['text'] ); ?></p>
                        <?php endif; ?>

                        <p class="int-ctas__link uppercase-title uppercase-title--has-arrow">

                            <?php echo esc_html( $cta['link']['title'] ); ?>

                            <span class="sw-arrow__wrapper">
                                <?php storage_warrior_svg( 'long-arrow-right', 'black' ); ?>
                            </span>
                        </p>
                    </a>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>
</main>

<?php
get_footer();
