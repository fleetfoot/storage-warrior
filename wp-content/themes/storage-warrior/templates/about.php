<?php
/**
 * Template Name: About
 *
 * @package Storage_Warrior
 */

$about_title = get_field( 'page_about_title' );

$section_1 = get_field( 'page_about_section_1' );
$section_2 = get_field( 'page_about_section_2' );

$sections = [ $section_1, $section_2 ];

$cta = get_field( 'page_about_cta' );

get_header();
?>

<main id="primary" class="site-main">
    <header class="sw-section sw-header">
        <div class="sw-container">
            <h1 class="sw-header__eyebrow"><?php the_title(); ?></h1>
            <h2 class="sw-header__title"><?php echo esc_html( $about_title ); ?></h2>
        </div>
    </header>

    <section class="sw-section">
        <div class="sw-container">
            <?php foreach ( $sections as $section ) : ?>
                <?php if ( $section['image'] && $section['content'] ) : ?>
                    <div class="sw-row">
                        <div class="sw-row__image">
                            <?php
                            echo wp_get_attachment_image(
                                $section['image'],
                                'large',
                                false,
                                [ 'loading' => 'lazy' ]
                            );
                            ?>
                        </div>

                        <div class="sw-row__content">
                            <?php echo wp_kses_post( $section['content'] ); ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </section>

    <?php if ( $cta['link'] ) : ?>
        <section class="sw-section">
            <div class="sw-container">
                <div class="int-ctas int-ctas--single">
                    <a href="<?php echo esc_url( $cta['link']['url'] ); ?>" class="int-ctas__cta">
                        <h5 class="int-ctas__title font-heading-sm"><?php echo esc_html( $cta['title'] ); ?></h5>
                        <?php if ( $cta['text'] ) : ?>
                            <p class="int-ctas__caption"><?php echo esc_html( $cta['text'] ); ?></p>
                        <?php endif; ?>

                        <p class="int-ctas__link uppercase-title uppercase-title--has-arrow">

                            <?php echo esc_html( $cta['link']['title'] ); ?>

                            <span class="sw-arrow__wrapper">
                                <?php storage_warrior_svg( 'long-arrow-right', 'black' ); ?>
                            </span>
                        </p>
                    </a>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>
</main>

<?php
get_footer();
