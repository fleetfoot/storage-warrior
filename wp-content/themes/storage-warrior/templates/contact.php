<?php
/**
 * Template Name: Contact
 *
 * @package Storage_Warrior
 */

$eyebrow        = get_field( 'page_contact_eyebrow' );
$contact_title  = get_field( 'page_contact_title' );
$subtitle       = get_field( 'page_contact_subtitle' );
$form_shortcode = get_field( 'page_contact_form_shortcode' );

get_header();
?>

<main id="primary" class="site-main">
    <header class="sw-section sw-header">
        <div class="sw-container">
            <h1 class="sw-header__eyebrow"><?php echo esc_html( $eyebrow ); ?></h1>
            <h2 class="sw-header__title"><?php echo esc_html( $contact_title ); ?></h2>
            <div class="sw-header__subtitle">
                <?php if ( $subtitle ) : ?>
                    <?php echo wp_kses_post( $subtitle ); ?>
                <?php endif; ?>
            </div>
        </div>
    </header>

    <section class="sw-section">
        <div class="sw-container">
            <?php echo do_shortcode( $form_shortcode ); ?>
        </div>
    </section>

    <?php get_template_part( 'template-parts/internal-ctas' ); ?>
    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>
</main>



<?php
get_footer();
