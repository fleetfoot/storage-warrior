<?php
/**
 * Template Name: Ebay
 *
 * @package Storage_Warrior
 */

$eyebrow       = get_field( 'page_ebay_eyebrow' );
$ebay_title    = get_field( 'page_ebay_title' );
$ebay_subtitle = get_field( 'page_ebay_subtitle' );
$ebay_caption  = get_field( 'page_ebay_caption' );
$shortcode     = get_field( 'page_ebay_an' );

get_header();
?>

<main id="primary" class="site-main">
    <header class="sw-section sw-header">
        <div class="sw-container">
            <h1 class="sw-header__eyebrow"><?php echo esc_html( $eyebrow ); ?></h1>
            <h2 class="sw-header__title"><?php echo esc_html( $ebay_title ); ?></h2>
            <?php if ( $ebay_subtitle ) : ?>
                <div class="sw-header__subtitle"><?php echo wp_kses_post( $ebay_subtitle ); ?></div>
            <?php endif; ?>
        </div>
    </header>

    <section class="sw-section">
        <div class="sw-container">

            <?php if ( $ebay_caption ) : ?>
                <p class="ebay-grid__caption"><?php echo wp_kses_post( $ebay_caption ); ?></p>
            <?php endif; ?>

            <div id="ebay-grid" class="ebay-grid">
                <?php echo do_shortcode( $shortcode ); ?>
            </div>

            <!-- Display if adblock is present -->
            <div id="link-to-ebay" class="link-to-ebay display-none align-center margin-top-large margin-bottom-large">
                <p class="font-body-xl">It looks like you have adblock enabled. Unfortunately we cannot display eBay items with adblock enabled. Please visit <a href="https://www.ebay.com/usr/storage_warriors">our shop at eBay.com</a></p>
            </div>
        </div>
    </section>

    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>
</main>



<?php
get_footer();
