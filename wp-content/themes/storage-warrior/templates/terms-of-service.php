<?php
/**
 * Template Name: Terms of Service
 *
 * @package Storage_Warrior
 */

get_header();

?>
<main id="primary" class="site-main">
    <header class="sw-section sw-header">
        <div class="sw-container">
            <h1 class="sw-header__title"><?php the_title(); ?></h1>
        </div>
    </header>

    <section class="sw-section">
        <div class="sw-container sw-container--border-bottom sw-content">
            <?php
            while ( have_posts() ) :
                the_post();

                the_content();

            endwhile;
            ?>
        </div>
    </section>

    <?php get_template_part( 'template-parts/newsletter-cta' ); ?>
</main>



<?php
get_footer();
